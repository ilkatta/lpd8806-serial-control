#ifndef string_utils_h
#define string_utils_h

#include "Arduino.h"

int split(String str, String *ret);
int count(String str,char o);
String clone(String str);

#endif
