#include "Arduino.h"
#include "buffer.h"
#include "string_utils.h"

Buffer::Buffer(){
  clear();
}

void Buffer::clear(){
  for (int i=0;i<BUFFER_SIZE;i++){
    _buffer[i] = ""; 
  }
  _b=0;
}

void Buffer::add(String cmd){
   if ( _b + 2 < BUFFER_SIZE ){
     _buffer[_b] = clone(cmd);
     _b++;
   }
}

String Buffer::get(){
  if ( available() ){
     _b--;
     String ret = clone(_buffer[_b]);
     _buffer[_b] = "";
     
     return ret;
  }
   
}

boolean Buffer::available(){
  return _b > 0;
}
