#include "LPD8806.h"
#include "SPI.h"
#include "buffer.h"
//#include "string_utils.h"

/* CONFIG */
#define NUM_LEDS 30
#define DATA_PIN 2
#define CLOCK_PIN 3
#define SERIAL_BAUD 9600
#define LOCAL_ECHO true

/* functions */
void setAll(uint32_t c);
void clearAll();


/* global vars */
String inputString;
Buffer buffer;
LPD8806 strip = LPD8806(NUM_LEDS, DATA_PIN, CLOCK_PIN);
int v;
void setup() {
  Serial.begin(SERIAL_BAUD);
  strip.begin();
  clearAll();
  v=0;
  inputString.reserve(200);
}

void loop() {
    v = v % 255;
    setAll(strip.Color(v,v,v));
    if (buffer.available()) {      
      String cmd = buffer.get();
      int value = cmd.toInt();
      if ( value > 0 || ( value == 0 && cmd.startsWith("0") ) ){
        if ( value <= 255 ) {
          v = value;
        }else{
          Serial.println(cmd + ": command unknown"); 
        }
      }else{
        Serial.println(cmd + ": command unknown");
      }
    }
}

void setAll(uint32_t c) {
  int i;
  for (i=0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, c); 
  }
  strip.show();
}

void clearAll(){
  for (int i=0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, 0);
  } 
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (LOCAL_ECHO){
      Serial.write(inChar);
    }
    if (inChar == '\n') {
      if (LOCAL_ECHO){
        //Serial.println();
      }
      buffer.add(inputString);
      inputString = "";
    }else{
      if  (  inChar != '\r' ){
          inputString += inChar;
      }
    }
  }
}

/*
int a[][21] = {
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0},
	{0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0},
	{1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,0,0,0,1,1,1},
	{1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1},
	{0,0,0,0,0,0,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1},
	{1,1,1,0,0,0,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1},
	{1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1},
	{0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0},
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
};
*/
