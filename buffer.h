#ifndef buffer_h
#define buffer_h

#define BUFFER_SIZE 100

#include "Arduino.h"
#include "string_utils.h"
class Buffer
{
  public:
    Buffer();
	void clear();
	void add(String cmd);
	String get();
	boolean available();
  private:
    String _buffer[BUFFER_SIZE];
    int _b;
};

#endif